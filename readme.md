To repozytorium zawiera dwa katalogi, z których każdy jest odrębnym projektem. 
Są one niezależne od siebie, dlatego najwygodniej po sklonowaniu repozytorium otworzyć je w 
środowisku NetBeans oddzielnie, wczytując z dysku.